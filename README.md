# Second RIoT Hackathon - source code examples #

sample code to interact with vizix, using nodejs and Arduino

### REST Endpoints ###

Using Nodejs

* getThing.js to get the header and the fields for any thing, you need the thingId in order to get the record
* getTimeseries.js to get the data timeseries for an specific thing


### Weather Station ###

An arduino project to use temperature and light sensors, and send every 2 seconds to Vizix using only MQTT  

Requirements:

* 1 arduino uno
* 1 arduino nano
* 2 Nrf24L01 rx/tx 
* 1 DHT11
* 1 light sensor
* 1 Ethernet Shield

Additional libraries: 

* [NRF24L01](http://playground.arduino.cc/InterfacingWithHardware/Nrf24L01) 
* [DHT11](http://playground.arduino.cc/Main/DHT11Lib)
* [PubSubClient](https://github.com/knolleary/pubsubclient)
* [NTPClient](http://playground.arduino.cc/Code/NTPclient)


### Source ###

* Weather Station 
this code goes in the Arduino Nano

* Weather Station Monitor
this is the code for the Arduino with the Ethernet shield
send mqtt messages using the Ethernet shield
and use NTPClient to sync the arduino's internal clock
