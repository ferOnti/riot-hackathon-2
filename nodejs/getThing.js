var http = require('http');
var net = require('net');
var url = require('url');

var thingId = 29;
var vizixHostname = 'vizix.hackiot.com';
var vizixPath = "/riot-core-services/api/thing/" + thingId + "/record";
var options = {
  hostname: vizixHostname,
  port: 8080,
  path: vizixPath,
  method: 'GET',
  headers: {
    'Content-Type': 'application/json',
    'Api_key': 'root',
  }
};

var req = http.request(options, function(res) {
  console.log('STATUS: ' + res.statusCode);
  console.log('HEADERS: ' + JSON.stringify(res.headers));
  res.setEncoding('utf8');
  var body = "";
  res.on('data', function (chunk) {
    body += chunk;
  });

  res.on('end', function() {
    console.log('Body:')
    var bodyJson = JSON.parse(body);

    displayThing (bodyJson);
  })
});

req.on('error', function(e) {
  console.log('problem with request: ' + e.message);
});

req.end();

var displayThing = function (thing) {

    console.log (thing);
}

