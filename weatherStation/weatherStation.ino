#include "SPI.h"
#include "RF24b.h"
#include <dht11.h>

#define ANALOG_LCD 1

DHT dht(A0);
int t = 0;

const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL };   // Radio pipe addresses for the 2 nodes to communicate.
byte data[32];                           //Data buffer for testing data transfer speeds
unsigned long counter, rxTimer;          //Counter and timer for keeping track transfer info
unsigned long startTime, stopTime;  
bool TX=1, RX=0, role=0;

RF24 radio(7,8);                        // Set up nRF24L01 radio on SPI bus plus pins 7 & 8


void setup() {
  Serial.begin(115200);
  Serial.println("\nWeather Station\n\n");
  delay(1000);//Wait 1000ms recommended delay before accessing sensor

  radio.begin();                           // Setup and configure rf radio
  radio.setChannel(1);
  radio.setPALevel(RF24_PA_MAX);
  radio.setDataRate(RF24_1MBPS);
  radio.setAutoAck(1);                     // Ensure autoACK is enabled
  radio.setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries
  
  radio.setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance

  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
  radio.stopListening();
  //radio.printDetails();                   // Dump the configuration of the rf unit for debugging
  
  
  //randomSeed(analogRead(0));              //Seed for random number generation
  
  radio.powerUp();                        //Power up the radio
  delay(1000);
  Serial.print("*** begin transmitting to the other node\n\r");
 
}

void generateMessage(int t, double humidity, double temperature, int light) {
  for(int i=0; i<32; i++) {
     data[i] = 0;               //fill buffer with zeroes
  }
  
  char strHumidity[10];
  char strTemperature[10];
  char text[32];

  dtostrf(humidity,    4, 2, strHumidity);
  dtostrf(temperature, 4, 2, strTemperature);

  sprintf(text, "%d,%s,%s,%d", t, strHumidity, strTemperature, light );
  for(int i=0; i<strlen(text); i++) {
     data[i] = text[i];               //fill buffer with zeroes
     //Serial.print( text[i]);
  }

  Serial.print( "message: " );
  for(int i=0; i<32; i++) {
     Serial.write( data[i]);
  }
  Serial.println( "" );
}


void loop() {
    t ++;

    dht.read11();
    int light = analogRead(ANALOG_LCD);
    /*
    Serial.print(t);
    Serial.print(" ");
    Serial.print("Current humidity = ");
    Serial.print(dht.humidity);
    Serial.print("%  ");
    Serial.print("temperature = ");
    Serial.print(dht.temperature); 
    Serial.print("C  ");
    Serial.print("Light intensity = ");
    Serial.print(light); 
    Serial.println("");
    */

    generateMessage (t, dht.humidity, dht.temperature, light );

    if (!radio.writeFast(&data,32)) {   //Write to the FIFO buffers        
        Serial.println ("failed payload");
    }
    delay (10);
    //This should be called to wait for completion and put the radio in standby mode after transmission, returns 0 if data still in FIFO (timed out), 1 if success
    if (!radio.txStandBy()) { 
      delay (100);
      //Serial.println("delay");
      //counter+=3; 
    } //Standby, block only until FIFO empty or auto-retry timeout. Flush TX FIFO if failed
   
    //radio.txStandBy(1000);              //Standby, using extended timeout period of 1 second
   
    //Serial.println("Message sent");
   
    delay(2000);
   }
