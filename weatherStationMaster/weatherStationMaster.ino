/** Weather Station Master
 * This scripts manages the Arduino Weather Station Master
 */

#include <SPI.h>
#include <Time.h>
#include <RF24.h>
#include "printf.h"
#include <EEPROM.h>
#include <EepromConfig.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <PubSubClient.h>
#include <NtpClient.h>

//globals
//const int timeZone = -4;
//unsigned int udfLocalPort = 8888;  // local port to listen for UDP packets
int times = 0;
int sequenceNumber = 0;
const uint64_t pipes[2] = { 0xABCDABCD71LL, 0x544d52687CLL };   // Radio pipe addresses for the 2 nodes to communicate.
const int timeZone = -4;

//ips
byte mac[]        = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xE0 };
byte ip[]         = { 0, 0, 0, 0 };
byte server[]     = { 0, 0, 0, 0 };
byte timeserver[] = { 0, 0, 0, 0 };

//class instances
EepromConfig config;
EthernetUDP Udp;
EthernetClient tcpClient;
PubSubClient client(tcpClient);
NtpClient ntpClient;
RF24 radio(8,9);

//data buffers
byte data[32];
char thingType[32];
char serialNumber[21];
char arduinoId[17];

//mqtt callback
void callback(char* topic, byte* payload, unsigned int length);

//overcharging strcat
void strcat(char* token, char appended)
{
    token[strlen(token)] = appended;
    token[strlen(token)+1] = 0;
}

void setup(void) {
  Serial.begin(115200);
  printf_begin();

  Serial.println ();
  Serial.println ( F("************************************************"));
  Serial.println ( F("*   setup arduino for Weather Station Project  *"));
  Serial.println ( F("************************************************"));

  config.add("arduinoId",   "Arduino ID", 0, 16);
  config.add("ip",          "ipAddress", 16, 16);
  config.add("ntpServer",   "NTP time server", 32, 16);
  config.add("mqttHost",    "mqtt host", 48, 16);
  //config.add("mqttPort",    "mqtt port", 64, 4);
  config.add("serialNumber","Vizix serialNumber", 69, 21);
  config.add("thingType",   "Vizix thingtype", 90, 32);
  delay (1000);

  config.showEeprom();
  delay (1000);

  config.getEepromVar(0,16, arduinoId);
  config.getIpFromEeprom(16,16, ip);
  config.getIpFromEeprom(32,16, timeserver);
  config.getIpFromEeprom(48,16, server);
  //config.getEepromVar(64, 5, mqttPort);
  config.getEepromVar(69,21, serialNumber);
  config.getEepromVar(90,32, thingType);


  if (Serial.available() > 0) {
    while (true) {
      Serial.print (F("\nselect an option to change:"));
      config.executeOptions();
    }
  }


  IPAddress mqttServer(server[0], server[1], server[2], server[3]);

  client.setServer(mqttServer, 1883);
  client.setCallback(callback);

  Ethernet.begin(mac, ip);
  delay (1500);

  Serial.print(F("Local IP: "));
  Serial.println ( Ethernet.localIP() );

  //try to sync to ntp server
  ntpClient.setTimeserver(timeserver);
  ntpClient.sync();
  ntpClient.digitalClockDisplay();

  radio.begin();                           // Setup and configure rf radio
  radio.setChannel(1);
  radio.setPALevel(RF24_PA_MAX);
  radio.setDataRate(RF24_1MBPS);
  radio.setAutoAck(1);                     // Ensure autoACK is enabled
  radio.setRetries(2,15);                  // Optionally, increase the delay between retries & # of retries

  radio.setCRCLength(RF24_CRC_8);          // Use 8-bit CRC for performance
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);

  radio.startListening();                 // Start listening

  char clientId[40];
  char strServer[16];
  strcpy (clientId, "mqtt-");
  strcat (clientId, arduinoId);
  if (client.connect(clientId)) {
    char greeting[48];
    strcpy (greeting, "hello world from ");
    strcat (greeting, arduinoId);
    client.publish("/bth/101/greetings", greeting);
    char topicSubscribe[48];
    strcpy( topicSubscribe, "/bth/101/");
    strcat( topicSubscribe, arduinoId);
    strcat( topicSubscribe, "/actor");
    client.subscribe(topicSubscribe);
    Serial.print(F("\n\rsubcribed to: "));
    Serial.print(topicSubscribe);
  }

  radio.powerUp();                        //Power up the radio
  radio.printDetails();                   // Dump the configuration of the rf unit for debugging

  //randomSeed(analogRead(0));              //Seed for random number generation

}

// Mqtt Callback function
void callback(char* topic, byte* payload, unsigned int length) {
  // In order to republish this payload, a copy must be made
  // as the orignal payload buffer will be overwritten whilst
  // constructing the PUBLISH packet.
  Serial.println ("callback ");

  // Allocate the correct amount of memory for the payload copy
  byte* p = (byte*)malloc(length +1);
  memset(p, 0, length +1);
  char* str = (char*)p;
  // Copy the payload to the new buffer
  memcpy(p,payload,length);
  //client.publish("/v1/data/helloworld", p, length);
  Serial.println (str );
  free(p);
}

void sendVizixMessage(byte *data)
{
    char token[32];
    char topic[64];
    char sn[12];
    char timestamp[16];
    char humidity[12];
    char temperature[12];
    char light[12];
    char body[256];

    memset( topic, 0, sizeof(topic));
    memset( body, 0, sizeof(body));
    memset( sn, 0, sizeof(sn));
    memset( timestamp, 0, sizeof(timestamp));
    memset( humidity, 0, sizeof(humidity));
    memset( temperature, 0, sizeof(temperature));
    memset( light, 0, sizeof(light));

    strcpy(topic, "/v1/data/ALEB/");
    strcat(topic, thingType);

    strcpy (token, "");
    int field = 0;
    //Serial.print(F("Received: "));
    for (int j=0; j<32; j++) {
        if (data[j] != 0) {
            //Serial.write(data[j]);
            if (data[j] == ',') {
              field++;
            } else {
              if (field == 0) {
                strcat(sn, data[j]);
              }
              if (field == 1) {
                strcat(humidity, data[j]);
              }
              if (field == 2) {
                strcat(temperature, data[j]);
              }
              if (field == 3) {
                strcat(light, data[j]);
              }
            }

        }

    }
    //Serial.println();

    strcpy(body, "sn,");
    strcat(body, sn);
    strcat(body, "\n");

    time_t currentTime;
    currentTime = now();
    currentTime += -1 * timeZone * SECS_PER_HOUR;

    ltoa (currentTime, timestamp, 10);
    strcat(timestamp, "000" );

    strcat(body, serialNumber);
    strcat(body, ",");
    strcat(body, timestamp);
    strcat(body, ",");
    strcat(body, "humidity");
    strcat(body, ",");
    strcat(body, humidity);
    strcat(body, "\n");

    strcat(body, serialNumber);
    strcat(body, ",");
    strcat(body, timestamp);
    strcat(body, ",");
    strcat(body, "temperature");
    strcat(body, ",");
    strcat(body, temperature);
    strcat(body, "\n");

    strcat(body, serialNumber);
    strcat(body, ",");
    strcat(body, timestamp);
    strcat(body, ",");
    strcat(body, "light");
    strcat(body, ",");
    strcat(body, light);
    strcat(body, "\n");



    Serial.print (F("sent to mqtt Topic: "));
    Serial.print (topic);
    Serial.print (" ");
    Serial.print (sn);
    Serial.print (": ");
    Serial.print (humidity);
    Serial.print (" ");
    Serial.print (temperature);
    Serial.print (" ");
    Serial.print (light);
    Serial.println();

    client.publish(topic, body );
}

void sendSimpleMqttMessage(byte *data)
{
    char token[32];
    char topic[64];
    char sn[12];
    char timestamp[16];
    char humidity[12];
    char temperature[12];
    char light[12];
    char body[256];

    memset( topic, 0, sizeof(topic));
    memset( body, 0, sizeof(body));
    memset( sn, 0, sizeof(sn));
    memset( timestamp, 0, sizeof(timestamp));
    memset( humidity, 0, sizeof(humidity));
    memset( temperature, 0, sizeof(temperature));
    memset( light, 0, sizeof(light));

    strcpy(topic, "/bth/bridge/");
    strcat(topic, arduinoId);

    strcpy (token, "");
    int field = 0;
    Serial.print(F("Received: "));
    for (int j=0; j<32; j++) {
        if (data[j] != 0) {
            Serial.write(data[j]);
            if (data[j] == ',') {
              field++;
            } else {
              if (field == 0) {
                strcat(sn, data[j]);
              }
              if (field == 1) {
                strcat(humidity, data[j]);
              }
              if (field == 2) {
                strcat(temperature, data[j]);
              }
              if (field == 3) {
                strcat(light, data[j]);
              }
            }

        }

    }
    Serial.println();

    strcat(body, "{");

    time_t currentTime;
    currentTime = now();
    currentTime += -1 * timeZone * SECS_PER_HOUR;

    ltoa (currentTime, timestamp, 10);

    strcat(body, "\"ts\":");
    //strcat(body, timestamp);
    strcat(body, "0");
    strcat(body, ",");
    strcat(body, "\"humidity\":");
    strcat(body, humidity);
    strcat(body, ",");
    strcat(body, "\"temperature\":");
    strcat(body, temperature);
    strcat(body, ",");
    strcat(body, "\"light\":");
    strcat(body, light);
    strcat(body, "");
    strcat(body, "}");

    Serial.print (F("sending message to mqtt: "));
    Serial.print (topic);
    Serial.println();
    Serial.print (body);
    Serial.println();
    client.publish(topic, body );
}

void loop(void){

    while(radio.available()) {
        memset(data, 0, sizeof(data));
        radio.read(&data,32);
        if (data[0] != 0) {
            sendVizixMessage( data );
            //sendSimpleMqttMessage( data );
        }
       client.loop();
    }

    delay(100);
    times++;
    if (times % 10) {
       client.loop();
    }
}
